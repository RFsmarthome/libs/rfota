#ifndef _RF_OTA_H
#define _RF_OTA_H

extern void otaInit(const String &hostname);
extern void otaLoop();

#endif
