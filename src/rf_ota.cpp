#include <Arduino.h>
#include <ArduinoOTA.h>

#include <uuid/log.h>

#include "rf_ota.h"

static uuid::log::Logger otaLogger(F("ota"));

void otaInit(const String &hostname)
{
    ArduinoOTA.setHostname(hostname.c_str());
    ArduinoOTA
    .onStart([]() {
        String type;
        if(ArduinoOTA.getCommand() == U_FLASH) {
            type = "sketch";
        } else {
            type = "filesystem";
        }
        otaLogger.debug("Start updateing %s", type);
    })
    .onEnd([]() {
        otaLogger.debug("Updated ended");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
        String msg;
        switch(error) {
        case OTA_AUTH_ERROR:
            msg = "Auth failed";
            break;
        case OTA_BEGIN_ERROR:
            msg = "Begin failed";
            break;
        case OTA_CONNECT_ERROR:
            msg = "Connect failed";
            break;
        case OTA_END_ERROR:
            msg = "End failed";
            break;
        case OTA_RECEIVE_ERROR:
            msg = "Receive error";
            break;
        }
        otaLogger.err("Update error[%u]: %s\n", error, msg);
    });

    ArduinoOTA.begin();
}

void otaLoop()
{
    ArduinoOTA.handle();
}
